
from glob import glob
import re
import time
import pandas as pd
import seaborn as sns; sns.set()
import matplotlib.pyplot as plt

def read(file):
    f = open(file, 'r')
    lines = f.readlines()
    f.close()
    # remove '\n' from end of lines
    return [line[:-1] for line in lines]

def parse_log(log):
    regex = '^(\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d+Z) \[.+\](?: \[\d+\])? \[?([A-Z]+)\]? in ([\w\d]+): (.+)$'
    match = re.search(regex, log)
    return match[1], match[2], match[3], match[4]

def load_files(files):
    file_contents = list(map(read, files))

if __name__ == "__main__":

    files = glob('./server_logs/*.log')