import os
import csv
import cv2
import shutil
import argparse
import numpy as np
from PIL import Image
from typing import Union
from pathlib import Path

import torch
import torchvision
import torch.optim
import torch.utils.data
import torch.nn.parallel
import torch.utils.data.distributed
import torch.backends.cudnn as cudnn
import torchvision.transforms as transforms

from deep_hrnet_yolov3.lib import models
from deep_hrnet_yolov3.tools import _init_paths
from deep_hrnet_yolov3.lib.utils.timer import timing
from deep_hrnet_yolov3.coco_constants import COCO_INSTANCE_CATEGORY_NAMES
from deep_hrnet_yolov3.lib.utils.transforms import get_affine_transform, box_to_center_scale

from config import cfg, update_config
from core.function import get_final_preds
from yolov3.models import Darknet
from yolov3.utils.utils import non_max_suppression, rescale_boxes, letterbox_image


class DeepHRNet:
    def __init__(self, yolo_config, yolo_weights, cfg_file, model_path, use_yolo=False):
        self.yolo_config = yolo_config
        self.yolo_weights = yolo_weights
        self.use_yolo = use_yolo

        # TODO: currently only single GPU
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        self.init_models(cfg_file, model_path)

    def init_models(self, cfg_file, model_path):
        # cudnn related setting
        cudnn.benchmark = cfg.CUDNN.BENCHMARK
        torch.backends.cudnn.deterministic = cfg.CUDNN.DETERMINISTIC
        torch.backends.cudnn.enabled = cfg.CUDNN.ENABLED
        update_config(cfg, cfg_file, model_path)

        # object detetcion model: YOLOv3 or Faster-RCNN
        if self.use_yolo:
            print('=> Loading model YOLOv3 for object detection...')
            self.box_model = Darknet(self.yolo_config).to(self.device)
            self.box_model.load_darknet_weights(self.yolo_weights)
            self.box_model.eval()
        else:
            print('=> Loading model Faster-RCNN (ResNet50-FPN) for object detection...')
            self.box_model = torchvision.models.detection.fasterrcnn_resnet50_fpn(pretrained=True).to(self.device)
            self.box_model.eval()

        # pose estimation model
        self.pose_model = eval('models.' + cfg.MODEL.NAME + '.get_pose_net')(
            cfg, is_train=False
        )

        if cfg.TEST.MODEL_FILE:
            print('=> Loading model from {}'.format(cfg.TEST.MODEL_FILE))
            self.pose_model.load_state_dict(torch.load(cfg.TEST.MODEL_FILE), strict=False)
        else:
            print('Expected model defined in config at TEST.MODEL_FILE')

        # TODO: multiple GPUs?
        self.pose_model = torch.nn.DataParallel(self.pose_model, device_ids=cfg.GPUS).to(self.device)

    # @timing
    def get_person_detection_boxes(self, model, img, conf_thres=0.5):
        pil_image = Image.fromarray(img)  # Load the image
        transform = transforms.Compose([transforms.ToTensor()])  # Defing PyTorch Transform
        transformed_img = transform(pil_image)  # Apply the transform to the image
        pred = model([transformed_img.to(self.device)])  # Pass the image to the model
        pred_classes = [COCO_INSTANCE_CATEGORY_NAMES[i]
                        for i in list(pred[0]['labels'].cpu().numpy())]  # Get the Prediction Score
        pred_boxes = [[(i[0], i[1]), (i[2], i[3])]
                      for i in list(pred[0]['boxes'].detach().cpu().numpy())]  # Bounding boxes
        pred_score = list(pred[0]['scores'].detach().cpu().numpy())
        if not pred_score:
            return []
        # Get list of index with score greater than threshold
        pred_t = [pred_score.index(x) for x in pred_score if x > conf_thres][-1]
        pred_boxes = pred_boxes[:pred_t + 1]
        pred_classes = pred_classes[:pred_t + 1]

        person_boxes = []
        for idx, box in enumerate(pred_boxes):
            if pred_classes[idx] == 'person':
                person_boxes.append(box)

        return person_boxes

    # @timing
    def get_person_detection_boxes_yolo(self, model, img, img_size=416, conf_thres=0.5,
                                        nms_thres=0.4):
        pil_image = Image.fromarray(img)  # Load the image
        pil_image = letterbox_image(pil_image, (img_size, img_size))  # Resize with padding, keep aspect ratio
        transform = transforms.Compose([transforms.ToTensor()])  # Defing PyTorch Transform
        transformed_img = transform(pil_image).unsqueeze_(0)  # batch_size=1 for test

        with torch.no_grad():
            outputs = model(transformed_img.type(torch.FloatTensor).to(self.device))
            outputs = non_max_suppression(outputs, conf_thres=conf_thres, nms_thres=nms_thres)

        # Rescale boxes to original image
        outputs_orig_size = rescale_boxes(outputs, img_size, img.shape[:2])
        if len(outputs_orig_size) > 0:
            outputs_orig_size = outputs_orig_size.detach().cpu().numpy()
            # filter only 'person' detections (idx=0 for yolo)
            outputs_orig_size = outputs_orig_size[np.where(outputs_orig_size[..., -1] == 0)[0]]

        return outputs_orig_size

    # @timing
    def get_pose_estimation_prediction(self, pose_model, image, center, scale):
        rotation = 0
        # pose estimation transformation
        trans = get_affine_transform(center, scale, rotation, cfg.MODEL.IMAGE_SIZE)
        model_input = cv2.warpAffine(
            image,
            trans,
            (int(cfg.MODEL.IMAGE_SIZE[0]), int(cfg.MODEL.IMAGE_SIZE[1])),
            flags=cv2.INTER_LINEAR)
        transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225]),
        ])

        # pose estimation inference
        model_input = transform(model_input).unsqueeze(0)
        # switch to evaluate mode
        pose_model.eval()
        with torch.no_grad():
            # compute output heatmap
            output = pose_model(model_input)
            preds, keypoint_scores = get_final_preds(
                cfg,
                output.clone(),
                np.asarray([center]),
                np.asarray([scale]))

        return preds, keypoint_scores

    def inference_image(self, image, pose_model, box_model, cfg, img_size,
                        conf_thres=0.8, nms_thres=0.4, use_yolo=False):
        all_keypoint_coord = []
        all_keypoint_scores = np.zeros((0, 17))

        # Object detection bounding boxes
        if use_yolo:
            pred_output = self.get_person_detection_boxes_yolo(box_model, image,
                                                               img_size=img_size,
                                                               conf_thres=conf_thres,
                                                               nms_thres=nms_thres)
            pred_boxes = [[tuple(box.squeeze()[:2]), tuple(box.squeeze()[2:4])] for box in pred_output]
        else:
            pred_boxes = self.get_person_detection_boxes(box_model, image, conf_thres=conf_thres)

        # Pose estimation
        for box in pred_boxes:
            center, scale = box_to_center_scale(box, cfg.MODEL.IMAGE_SIZE[0],
                                                cfg.MODEL.IMAGE_SIZE[1])
            image_pose = image.copy()
            pose_preds, keypoint_scores = self.get_pose_estimation_prediction(pose_model, image_pose,
                                                                              center, scale)

            cur_keypoint = []
            for _, mat in enumerate(pose_preds[0]):
                x_coord, y_coord = mat[0], mat[1]
                cur_keypoint.extend([y_coord, x_coord])
            all_keypoint_coord.append(cur_keypoint)
            all_keypoint_scores = np.vstack((all_keypoint_scores, keypoint_scores.reshape((1, 17))))

        all_keypoint_coord = np.array(all_keypoint_coord).reshape((-1, 17, 2))
        # The pose confidence score is the mean of the scores of the keypoints.
        pose_score = np.mean(all_keypoint_scores, axis=1)

        return pose_score, all_keypoint_coord, all_keypoint_scores

    def run(self, input_file: Union[str, np.ndarray],
             conf_thres: float = 0.8, nms_thres: float = 0.4, img_size: int = 416):

        if isinstance(input_file, str):
            input_file = cv2.cvtColor(cv2.imread(input_file), cv2.COLOR_BGR2RGB)
        pose_score, all_keypoint_coord, all_keypoint_scores = \
            self.inference_image(input_file, self.pose_model, self.box_model, cfg, img_size,
                                 conf_thres=conf_thres, nms_thres=nms_thres,
                                 use_yolo=self.use_yolo)

        return pose_score, all_keypoint_coord, all_keypoint_scores
