from pathlib import Path


# Deep HRNet config
coco_name_path = Path('deep_hrnet_yolov3/lib/config/coco.names')
pytorch_cfg_file = Path('deep_hrnet_yolov3/inference-config-coco.yaml')
pytorch_model_path = Path('deep_hrnet_yolov3/models/pose_coco/pose_hrnet_w32_384x288.pth')
yolo_config = Path('deep_hrnet_yolov3/lib/yolov3/config/yolov3.cfg')
yolo_weights = Path('deep_hrnet_yolov3/lib/yolov3/weights/yolov3.weights')
yolo_conf_thres = 0.8
yolo_nms_thres = 0.4