import time
from functools import wraps


def timing(func):
    @wraps(func)
    def wrap(*args, **kwargs):
        timer = Timer()
        timer.start()
        result = func(*args, **kwargs)
        time_used = timer.end()
        print(f'func: {func.__name__} time used: {time_used:.3f} seconds\n')
        # print(f'func: {func.__name__} args:[{args}, {kwargs}] took: {time_used:.2f} sec')
        return result
    return wrap


class Timer:
    def __init__(self):
        self.clock = {}

    def start(self, key="default"):
        self.clock[key] = time.time()

    def end(self, key="default"):
        if key not in self.clock:
            raise Exception(f"{key} is not in the clock.")
        interval = time.time() - self.clock[key]
        del self.clock[key]

        return interval
