from face_model import FaceModel
import os
import glob
import cv2
import line_profiler
import numpy as np 

if __name__ == "__main__":
    print(os.getcwd())
    path = os.path.join(os.getcwd(), 'data/office/')
    model = FaceModel()
    db_faces = np.zeros((10, 128), dtype=np.float)
    db_detected = [False] * 30
    temp_faces = dict()

    def match(face_vector):
        face_threshold = 0.36 #np.array([0.56, 0.48, 0.42, 0.36]) 
        best_match = None
        for db_face in db_faces:
            dist = np.linalg.norm(np.asarray(db_face) - np.asarray(face_vector))
            print('distance' , dist)
            if dist < face_threshold:
                if not best_match or dist < best_match[0]:
                    best_match = dist, db_face
        return best_match and best_match[1]

    def match_or_record(face_vector):
        face = match(face_vector)
        if face == None:
            db_faces[index] = face
            print('saving to memory')
            return face
        else:
            print('found a match')
    
    def process_image(image):
        new_face_vector, score, height = model.execute(image)
        if len(new_face_vector) == 0:
            return 0
        else: 
            return new_face_vector
    
    def update_vector(old_vector, new_vector, num_obs):
        average_face_vector = np.asarray(old_vector)
        average_face_vector *=  num_obs
        average_face_vector = np.add(average_face_vector, new_vector) 
        average_face_vector /= num_obs + 1
        return average_face_vector

    file_names = sorted(os.listdir(path), key=lambda f: int(f.split('-')[0]))
    for f in file_names: 
        print(f)
        temp_id = f.split('-')[2]
        img = cv2.imread(path+f)
        new_face_vector = process_image(img)
        print(new_face_vector)
        if new_face_vector: 
            print('face found')
            if temp_id not in temp_faces:
                print('image not in temp table')
                temp_data = {
                    'num_obs' : 1,
                    'vector' : new_face_vector
                }
                temp_faces[temp_id] = temp_data
            else:
                temp_data = temp_faces[temp_id]
                if temp_data['num_obs'] < 4:
                    updated_vector = update_vector(temp_data['vector'], new_face_vector, temp_data['num_obs'])
                    temp_data['num_obs'] += 1
                    temp_data['vector'] = updated_vector
                    temp_faces[temp_id] = temp_data
                    if temp_data['num_obs'] == 4:
                        print('got enough observations')
                        index = db_detected.index(False)
                        db_faces[index] = updated_vector
                        db_detected[index] = True                  
        else:
            print('no detection')
    print(db_detected)

        
        