import os
import glob
import cv2
import numpy as np
import time
import argparse
from pipeline import Pipeline, Termination, WorkerPoolDescription

models = {
    'cnn' : lambda: FaceDetCNN(),
    'dnn' : lambda: FaceDetDNN(),
    'mtcnn' : lambda: FaceDetMTCNN(),
    'facenet' : lambda: FaceNet(),
    'openface' : lambda: OpenFace()
}

def log(filename, num_of_workers, proc_time, status=''):
    with open('benchmarks_gpu_num_workers/%s_%s.csv' % (filename, str(num_of_workers)), 'a+') as f:
        f.write('%f%s\n' % (proc_time, status))

def time_op(f):
    start_time = time.time()
    x = f()
    end_time = time.time()
    return x, end_time - start_time

class DetectionResult:
    pass

class DetectionFailure(DetectionResult):
    def __init__(self, detection_time):
        self.detection_time = detection_time

class DetectionSuccess(DetectionResult):
    def __init__(self, detection, detection_time):
        self.detection = detection
        self.detection_time = detection_time

class RecognitionResult:
    def __init__(self, recognition, detection_time, recognition_time):
        self.recognition = recognition
        self.detection_time = detection_time
        self.recognition_time = recognition_time

class DetectionWorker:
    def __init__(self, modelname):
        print('initializing detection worker')
        self.modelname = modelname
        if modelname =='mtcnn':
            config = tf.ConfigProto()
            config.gpu_options.allow_growth = True
            self.graph = tf.Graph().as_default()
            self.sess = tf.Session(config=config)
            self.model = models[modelname]()
        else:
            self.model = models[modelname]()

    def __del__(self):
        if self.modelname == 'mtcnn':
            self.sess.close()
            tf.reset_default_graph()

    def work(self, image):
        detection, detection_time = time_op(lambda: self.model.execute(image))
        if len(detection):
            return DetectionSuccess(detection, detection_time)
        else:
            raise Termination(DetectionFailure(detection_time))

class RecognitionWorker:
    def __init__(self, modelname):
        print('initializing recognition worker')
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        self.graph = tf.Graph().as_default()
        self.sess = tf.Session(config=config)
        self.model = models[modelname]()
        #for the recognition models, they lazily load the tensorflow during init, so i have to execute the model to load the model
        #the workers sync happens on the shared memory event
        self.model.execute(np.zeros((28, 29, 3)))

    def __del__(self):
        self.sess.close()
        tf.reset_default_graph()

    def work(self, detection_result):
        recognition, recognition_time = time_op(lambda: self.model.execute(detection_result.detection))
        return RecognitionResult(recognition, detection_result.detection_time, recognition_time)

if __name__ == "__main__":
    #parse args before long tensorflow imports
    parser = argparse.ArgumentParser()
    parser.add_argument('detection_workers', type=int, help='number of detection workers', choices=[1,2,3,4,5])
    parser.add_argument('detection', type=str, help='detection algorithm to use', choices=['cnn', 'dnn', 'mtcnn'])
    parser.add_argument('recognition_workers', nargs='?', type=int, default = 0, help='number of recognition workers', choices=[0,1,2,3,4])
    parser.add_argument('recognition', nargs='?', default='', type=str, help='recognition algorithm to use {facenet, openface}', choices=['facenet', 'openface'].append(''))
    parser.add_argument('--gpu', help='Run on GPU', action='store_true') #not hooked up yet

    args = parser.parse_args()

    from keras import backend as K
    import tensorflow as tf
    from face_det_dnn import FaceDetDNN
    from face_det_cnn import FaceDetCNN
    from face_det_mtcnn import FaceDetMTCNN
    from face_rec_facenet import FaceNet
    from face_rec_openface import OpenFace

    det_model_name = args.detection
    rec_model_name = False
    if args.recognition is not '':
        rec_model_name = args.recognition

    num_detection_workers = args.detection_workers
    num_recognition_workers = args.recognition_workers
    path = os.path.join(os.getcwd(), 'faces/')
    filenames = sorted(os.listdir(path), key=lambda f: int(f.split('-')[0]))

    #detection pool
    pools = [WorkerPoolDescription(DetectionWorker, [det_model_name], num_detection_workers)]
    #recognition pool
    if rec_model_name and num_recognition_workers:
        pools.append(WorkerPoolDescription(RecognitionWorker, [rec_model_name], num_recognition_workers))


    pipeline = Pipeline(pools)

    #loop through images and submit them
    start_time = time.time()
    for filename in filenames:
        temp_id = filename.split('-')[2]
        order = filename.split('-')[0]
        img = cv2.imread(path + filename)

    #read results 
    results = []
    for _ in range(len(filenames)):
        results.append(pipeline.output())
    print(results)

    end_time = time.time()
    tot_time = end_time-start_time
    det_time = 0
    rec_time = 0
    num_of_jobs = len(results)

    #logging
    ##TODO: Cleanup logging code, shorten it, it is long and confusing now
    prefix = det_model_name + ('*' + rec_model_name if rec_model_name else '') + '_'
    for result in results:
        if isinstance(result, DetectionSuccess):
            print('Detection time', result.detection_time, ',success')
            log(prefix + det_model_name, num_detection_workers, result.detection_time, ',success')
            det_time += result.detection_time
        elif isinstance(result, DetectionFailure):
            print('Detection time', result.detection_time, ',failed')
            log(prefix + det_model_name, num_detection_workers, result.detection_time, ',failed')
        elif isinstance(result, RecognitionResult):
            print('Detection time', result.detection_time, ',success')
            print('Recognition time', result.recognition_time)
            log(prefix + det_model_name, num_detection_workers, result.detection_time)
            log(prefix  + rec_model_name, num_recognition_workers, result.recognition_time)
            det_time += result.detection_time
            rec_time += result.recognition_time

    runtime_file = 'runtime_' + prefix + '_' + str(num_detection_workers) + '_' + str(num_recognition_workers)
    p = [(det_time/num_of_jobs), (rec_time/num_of_jobs), tot_time, (tot_time/num_of_jobs), num_of_jobs]

    print('jobs processed %d with %d detection workers and %d recognition workers' %(num_of_jobs, num_detection_workers, num_recognition_workers))
    print('avg detection time : %f' %(p[0]))
    print('avg recognition time : %f' %(p[1]))
    print('total runtime : %f' %(p[2]))
    print('time per job : %f' %(p[3]))

    with open('benchmarks_gpu_num_workers/%s.csv' % runtime_file, 'a+') as f:
        f.write('avg_det_time,avg_rec_time,tot_time,time_per_job,num_of_jobs\n')
        f.write(','.join(list(map(str, p))))

