import os
import glob
import cv2
import numpy as np 
import time
import argparse
import csv
import dlib

detection_models = {
'cnn' : lambda: FaceDetCNN(),
'dnn' : lambda: FaceDetDNN(),
'mtcnn' : lambda: FaceDetMTCNN(),
'frontal' : lambda: FaceDetFrontal(),
}

recognition_models = {
'facenet' : lambda: FaceNet(),
'openface' : lambda: OpenFace(),
'resnet' : lambda: Resnet(),
}

dataset = {
    'office': {
        'path': 'data/office/',
        'names': 'data/office_names.csv',
        'anchor_images': 
        {
            'sondre': '51-80-3b-13.png',
            'ryan' : '28-30-4a-6.png',
            'viet' : '45-65-0d-7.png' ,
            'lawrence' : '98-220-2a-28.png' ,
        },
    },
    'celebrity':
    {
        'path': 'data/celebrity/',
        'names': 'data/celebrity_names.csv',
        'anchor_images': 
        {
            'jennifer': 'jen1.jpeg',
            'nicholas' : 'nic1.jpeg',
            'halle' : 'Halle_Berry_0006.jpg',
            'clint' : 'Clint_Eastwood_0005.jpg',
        }
    }
}

def l2_normalize(x):
    return x / np.sqrt(np.sum(np.multiply(x, x)))

def get_distance(anchor_face, face_vector):
    return np.sqrt(np.sum(np.square(np.subtract(np.asarray(anchor_face), np.asarray(face_vector)))))
    #return np.linalg.norm(np.asarray(anchor_face) - np.asarray(face_vector))

#the mtcnn, cnn and dnn models don't return a perfect square
def calculate_square_rectangle(rect):
    height = rect.bottom() - rect.top()
    width = rect.right() - rect.left()
    sq = max(height, width)
    cx = rect.left() + width // 2
    cy = rect.top() + height // 2
    return dlib.rectangle(cx - sq // 2, cy - sq // 2, cx + sq // 2, cy + sq // 2)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('detection', type=str, help='detection algorithm to use', choices=detection_models.keys())
    parser.add_argument('recognition', type=str, help='recognition algorithm to use', choices=list(recognition_models.keys()))
    parser.add_argument('dataset', type=str, help='dataset options', choices=list(dataset.keys()))
    args = parser.parse_args()

    from models.detection.dnn import FaceDetDNN
    from models.detection.cnn import FaceDetCNN
    from models.detection.mtcnn import FaceDetMTCNN
    from models.detection.frontal import FaceDetFrontal
    from models.recognition.facenet import FaceNet
    from models.recognition.openface import OpenFace
    from models.recognition.resnet import Resnet

    detection = detection_models[args.detection]()
    recognition = recognition_models[args.recognition]()
    dataset_name = args.dataset
    data =  dataset[dataset_name]
    
    print('** running %s - %s on %s data' % (args.detection, args.recognition, args.dataset))
    path = os.path.join(os.getcwd(),data['path'])

    names_dict = dict()
    reader = csv.reader(open(os.path.join(os.getcwd(),data['names']), 'r'))
    for row in reader:
        k, v = row
        names_dict[k] = v

    anchor_dict = data['anchor_images']
    anchor_images = dict()
    for name, file_name in anchor_dict.items():
        input_img = cv2.imread(path + file_name)
        input_img = cv2.cvtColor(input_img, cv2.COLOR_BGR2RGB)
        img, rect, confidence = detection.execute(input_img)
        if len(img):
            result = recognition.execute(img, rect)
            result = l2_normalize(result)
            anchor_images[name] = result

    num_of_detections = 0
    recognition_results = list()
    for file_name in os.listdir(path): 
        name = names_dict[file_name]
        input_img = cv2.imread(path + file_name)
        input_img = cv2.cvtColor(input_img, cv2.COLOR_BGR2RGB)
        img, rect, confidence = detection.execute(input_img)
        if isinstance(rect, dlib.rectangle):
            num_of_detections += 1
            if args.recognition != 'openface':
                rect = calculate_square_rectangle(rect)
            # img_annotation = img.copy()
            # cv2.rectangle(img_annotation, (rect.left(), rect.top()), (rect.right(), rect.bottom()), (0, 0, 255), 1)
            # cv2.rectangle(img_annotation, (square_rect.left(), square_rect.top()), (square_rect.right(), square_rect.bottom()), (0, 255, 0), 1)
            # cv2.imwrite('data/cropped/' + args.detection + '/' + args.dataset + '/*_' + file_name, img_annotation)
            result = recognition.execute(img, rect)
            result = l2_normalize(result)
            for anchor_name, anchor_image in anchor_images.items():
                dist = get_distance(anchor_image, result)
                recognition_results.append((dist, 1 if name == anchor_name else 0))
    with open('results/%s_%s_%s_%s.csv' % (args.detection, args.recognition, args.dataset, str(num_of_detections)), 'w+') as out:
        csv_writer = csv.writer(out)
        csv_writer.writerow(['distance','identification'])
        for row in recognition_results:
            csv_writer.writerow(row)