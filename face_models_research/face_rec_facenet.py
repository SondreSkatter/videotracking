from keras.models import load_model
import mtcnn
import numpy as np
import cv2

#https://github.com/nyoki-mtl/keras-facenet
#Input image: color, 160x160 images
#FaceNet model doesn't need alignment
class FaceNet:
    model_file = './models/facenet/model/facenet_keras.h5'

    def __init__(self):
        self.model = load_model(self.__class__.model_file)
    
    def scale_image(self, img):
        img = img.astype('float32')
        # image needs to be standardized for this model
        mean, std = img.mean(), img.std()
        img = (img - mean) / std
        img = np.expand_dims(img, axis=0)
        return img

    def execute(self, img):
        img = cv2.resize(img,(160,160))
        scaled_img = self.scale_image(img)
        embeddings = self.model.predict(scaled_img)
        #print('embedding found')
        if len(embeddings):
            return embeddings[0]
        return []
