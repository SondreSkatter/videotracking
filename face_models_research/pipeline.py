import sys
import os
import multiprocessing
from functools import reduce

sys.path.insert(0, '..')
os.environ['CUDA_VISIBLE_DEVICES'] = '0'
from utils.custom_queue import Queue

class Termination(Exception):
    def __init__(self, data):
        self.data = data

def run_worker(
    init_counter,
    worker_class,
    init_args,
    input_queue,
    continuation_queue,
    termination_queue
):
    worker = worker_class(*init_args)
    init_counter.increment()
    while True:
        try:
            continuation_queue.put(worker.work(input_queue.get()))
        except Termination as e:
            termination_queue.put(e.data)

class WorkerPoolDescription:
    def __init__(self, worker_class, init_args, num_processes):
        self.worker_class = worker_class
        self.init_args = init_args
        self.num_processes = num_processes

#Shared counter to initialize all the workers and sync them. It is a shared counter where each worker increments. 
class SharedCounter:
    def __init__(self, initial_value):
        self.event = multiprocessing.Event()
        self.counter = multiprocessing.Value('I', initial_value)

    def increment(self):
        with self.counter.get_lock():
            self.counter.value += 1
        self.event.set()

    # don't call wait_until in parallel or it will break
    def wait_until(self, listener):
        while True:
            self.event.clear()
            self.event.wait()
            if listener(self.counter.value):
                break

#This pipeline follows a continuation passing style pipeline with early termination. 
#It basically forwards the detection result to the continuation pool if it is detection is a success or it writes it to the result queue
#If there is no recognition flag in the program execution, there is no continuation. It returns the detection results
class Pipeline:
    def __init__(self, worker_pool_descs):
        num_workers = reduce(
            lambda acc, desc: acc + desc.num_processes,
            worker_pool_descs,
            0
        )
        init_counter = SharedCounter(0)
        self.continuation_queues = [Queue() for _ in worker_pool_descs]
        self.result_queue = Queue()

        def spawn_worker(worker_pool_desc, input_queue, output_queue):
            args = (
                init_counter,
                worker_pool_desc.worker_class,
                worker_pool_desc.init_args,
                input_queue,
                output_queue,
                self.result_queue
            )
            return multiprocessing.Pool(
                worker_pool_desc.num_processes,
                run_worker,
                args
            )

        self.worker_pools = []
        for i in range(len(worker_pool_descs)-1):
            worker = spawn_worker(
                worker_pool_descs[0],
                self.continuation_queues[i],
                self.continuation_queues[i+1]
            )
            self.worker_pools.append(worker)
        worker = spawn_worker(
            worker_pool_descs[-1],
            self.continuation_queues[-1],
            self.result_queue
        )
        self.worker_pools.append(worker)

        print('waiting for workers to initialize')
        init_counter.wait_until(lambda n: n >= num_workers)
        print('workers have initialized')

    def input(self, data):
        self.continuation_queues[0].put(data)

    def output(self):
        return self.result_queue.get()
