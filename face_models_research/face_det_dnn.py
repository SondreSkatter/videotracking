
import cv2
import numpy as np

#https://github.com/opencv/opencv/tree/master/samples/dnn/face_detector
class FaceDetDNN:
    config_file = './models/opencv-dnn/opencv_face_detector.pbtxt'
    model_file = './models/opencv-dnn/opencv_face_detector_uint8.pb'
    annotations_directory = 'face_det_results_dnn/'

    def __init__(self):
        self.detector_model = cv2.dnn.readNetFromTensorflow(self.__class__.model_file, self.__class__.config_file)
    
    def execute(self, img):
        scale_factor = 2.0
        mean_val = [104, 117, 123]
        blob = cv2.dnn.blobFromImage(cv2.resize(img, (300,300)), scale_factor, (300, 300), mean_val)
        self.detector_model.setInput(blob)
        detections = self.detector_model.forward()
        confidence = detections[0, 0, 0, 2] #getting the max confidence
        if confidence:
            #print('confidence ', confidence)
            (h, w) = img.shape[:2]
            box = detections[0, 0, 0, 3:7] * np.array([w, h, w, h])
            (x1, y1, x2, y2) = box.astype("int")
            
            #annotate image
            # img_annotation  = img.copy()
            # text = "{:.2f}%".format(confidence * 100)
            # y_label = y1 - 10 if y1 - 10 > 10 else y1 + 10
            # cv2.rectangle(img_annotation, (x1, y1), (x2, y2), (0, 0, 255), 1)
            # cv2.putText(img_annotation, text, (x1, y_label), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 1)
            # cv2.imwrite(self.__class__.annotations_directory + filename, img_annotation)

            ##crop image
            img = img[y1:y2, x1:x2]
            return img
        #print('no face found')
        return []