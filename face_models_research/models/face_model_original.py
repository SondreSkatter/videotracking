import cv2
import dlib

#brought the face model to the API directory 
class FaceModelOriginal():
    predictor_model = './models/face_id/shape_predictor_68_face_landmarks.dat'
    face_rec_model_path = './models/face_id/dlib_face_recognition_resnet_model_v1.dat'
    num_face_dims = 128

    def __init__(self):
        self.face_detector = dlib.get_frontal_face_detector()
        self.face_pose_predictor = dlib.shape_predictor(self.__class__.predictor_model)
        self.facerec = dlib.face_recognition_model_v1(self.__class__. face_rec_model_path)

    def execute(self, img):
        upSampleFactor = 2
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        detected_face,scores,idx = self.face_detector.run(gray, upSampleFactor, 0.0)
        print('orig' , detected_face[0])
        if len(detected_face) > 0: 
            print('rect' ,thisRect)
            pose_landmarks = self.face_pose_predictor(img, detected_face[0])
            newFaceData = self.facerec.compute_face_descriptor(img, pose_landmarks)
            return newFaceData
        return []
