##Model is taken from
##https://github.com/krasserm/face-recognition
##image needs to be aligned

from models.external.openface.model import create_model
from models.external.openface.align import AlignDlib
import cv2
import numpy as np

class OpenFace:
    weights_file = 'models/weights/openface/nn4.small2.v1.h5'
    align_landmarks_file = 'models/weights/openface/landmarks.dat'
    def __init__(self):
        self.model = create_model()
        self.model.load_weights(self.__class__.weights_file)
        self.aligner = AlignDlib(self.__class__.align_landmarks_file)

    def scale_image(self,img):
        return (img / 255.).astype(np.float32)

    def align_image(self, img):
        img_dims = 96
        bd_box = self.aligner.getLargestFaceBoundingBox(img)
        img_aligned = self.aligner.align(img_dims, img, bd_box, landmarkIndices=AlignDlib.OUTER_EYES_AND_NOSE)
        if img_aligned is not None:
            return self.scale_image(img_aligned)
        else:
            img = cv2.resize(img,(img_dims,img_dims))
            return self.scale_image(img)

    def execute(self, img, rect):
        img = img[rect.top():rect.bottom(), rect.left():rect.right()]
        img = self.align_image(img)
        embedding = self.model.predict(np.expand_dims(img, axis=0))
        if len(embedding):
            return embedding
        return []
