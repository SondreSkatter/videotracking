import dlib

class Resnet():
    predictor_model = 'models/weights/face_id/shape_predictor_68_face_landmarks.dat'
    face_rec_model_path = 'models/weights/face_id/dlib_face_recognition_resnet_model_v1.dat'

    def __init__(self):
        self.face_pose_predictor = dlib.shape_predictor(self.__class__.predictor_model)
        self.model = dlib.face_recognition_model_v1(self.__class__. face_rec_model_path)

    def execute(self, img, rect):
        pose_landmarks = self.face_pose_predictor(img, rect)
        newFaceData = self.model.compute_face_descriptor(img, pose_landmarks)
        return newFaceData
