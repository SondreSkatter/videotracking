from keras.models import load_model
import mtcnn
import numpy as np
import cv2
from models.external.openface.align import AlignDlib

#https://github.com/nyoki-mtl/keras-facenet
#Input image: color, 160x160 images
#FaceNet model doesn't need alignment
class FaceNet:
    model_file = 'models/weights/facenet/model/facenet_keras.h5'
    align_landmarks_file = 'models/weights/openface/landmarks.dat'

    def __init__(self):
        self.model = load_model(self.__class__.model_file)
        self.aligner = AlignDlib(self.__class__.align_landmarks_file)
    
    # image needs to be standardized for this model
    def standardize_image(self, img):
        mean, std = img.mean(), img.std()#np.mean(img), np.std(img)
        img = (img - mean) / std
        img = np.expand_dims(img, axis=0)
        return img

    #alignment doesn't improve accuracy
    def align_image(self, img):
        img_dims = 160
        bd_box = self.aligner.getLargestFaceBoundingBox(img)
        img_aligned = self.aligner.align(img_dims, img, bd_box, landmarkIndices=AlignDlib.OUTER_EYES_AND_NOSE)
        if img_aligned is not None:
            return self.standardize_image(img_aligned)
        else:
            img = cv2.resize(img,(img_dims,img_dims))
            return self.standardize_image(img)

    def execute(self, img, rect):
        #crop image
        img = img[rect.top():rect.bottom(), rect.left():rect.right()]
        img = cv2.resize(img,(160,160))
        img = self.standardize_image(img)
        #img = self.align_image(img)
        embeddings = self.model.predict(img)
        if len(embeddings):
            return embeddings[0]
        return []
