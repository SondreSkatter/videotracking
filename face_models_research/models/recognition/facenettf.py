
import tensorflow as tf
import models.external.facenettf.facenettf as facenet
import numpy as np
import cv2

# this example is taken from with edits https://github.com/dkarunakaran/facenet_mtcnn_tensorflow_inference_engine
class FacenetTF:
    def __init__(self):
        self.model_dir = 'models/weights/facenettf/20180402-114759/'
        self.image_size = 160
        self.images_placeholder = None
        self.phase_train_placeholder=None
        self.margin = 44

    def get_embedding(self, processed_img):
        reshaped = processed_img.reshape(-1, self.image_size, self.image_size, 3)
        feed_dict = {self.images_placeholder:reshaped, self.phase_train_placeholder:False }
        feature_vector = self.sess.run(self.embeddings, feed_dict=feed_dict)
        return feature_vector

    #this is meant to be used with multiple images in one session, however my workflow is different. Right now it is loading the model everytime it needs to run the recognition. I am leaving this as is right now, since it a bonus model i am trying out
    def execute(self, img, rect):
        with tf.Graph().as_default():
            with tf.Session() as sess:
                self.sess = sess
                facenet.load_model(self.model_dir)

                images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
                self.images_placeholder = tf.image.resize_images(images_placeholder,(self.image_size, self.image_size))
                self.embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
                self.phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")

                img_size = np.asarray(img.shape)[0:2]
                bounding_box_size = (rect[2]-rect[0])*(rect[3]-rect[1])
                img_center = img_size / 2
                offsets = np.vstack([ (rect[0]+rect[2])/2-img_center[1], (rect[1]+rect[3])/2-img_center[0] ])
                offset_dist_squared = np.sum(np.power(offsets,2.0),0)
                index = np.argmax(bounding_box_size-offset_dist_squared * 2.0) #not doing anything with this yet

                bb = np.zeros(4, dtype=np.int32)
                bb[0] = np.maximum(rect[0]-self.margin/2, 0)
                bb[1] = np.maximum(rect[1]-self.margin/2, 0)
                bb[2] = np.minimum(rect[2]+self.margin/2, img_size[1])
                bb[3] = np.minimum(rect[3]+self.margin/2, img_size[0])
                cropped = img[bb[1]:bb[3],bb[0]:bb[2],:]
                resized = cv2.resize(cropped, (self.image_size, self.image_size),interpolation=cv2.INTER_CUBIC)
                prewhitened = facenet.prewhiten(resized)
                return self.get_embedding(prewhitened)