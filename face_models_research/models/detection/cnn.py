import cv2
import dlib

#http://dlib.net/cnn_face_detector.py.html
class FaceDetCNN:
    model_file = 'models/weights/opencv-cnn/mmod_human_face_detector.dat'
    annotations_directory = 'data/detection_annotations/cnn/'
    def __init__(self):
        self.detector_model = dlib.cnn_face_detection_model_v1(self.__class__.model_file)

    def execute(self, img):
        upsample = 2
        faceRects = self.detector_model(img, upsample)
        if len(faceRects) > 0:
            x1 = faceRects[0].rect.left()
            y1 = faceRects[0].rect.top()
            x2 = faceRects[0].rect.right()
            y2 = faceRects[0].rect.bottom()
            confidence = faceRects[0].confidence
            rect = dlib.rectangle(faceRects[0].rect)

            #annotate image for seeing the results
            # img_annotation = img.copy()
            # text = "{:.2f}%".format(confidence * 100)
            # y_label = y1 - 10 if y1 - 10 > 10 else y1 + 10
            # cv2.rectangle(img_annotation, (x1, y1), (x2, y2), (0, 0, 255), 1)
            # cv2.putText(img_annotation, text, (x1, y_label), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 1)
            # cv2.imwrite(self.__class__.annotations_directory + filename, img_annotation)
        
            #crop image
            #img = img[y1:y2, x1:x2]
            return img, rect, confidence
        #print('no face detected')
        return [], [], 0