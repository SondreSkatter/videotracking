
import cv2
import dlib
import numpy as np
from models.external.mtcnn.align_mtcnn import AlignMTCNN

#pretrained model is taken from
#https://github.com/wangbm/MTCNN-Tensorflow
class FaceDetMTCNNTF:
    annotations_directory = 'data/detection_annotations/mtcnntf/'
    def __init__(self):
        self.detector_model = AlignMTCNN()
    
    def execute(self, img):
        boxes, points = self.detector_model.get_bounding_boxes(img)
        if len(boxes):
            rect = np.asarray(boxes[0][:-1], dtype=np.int32)
            confidence = boxes[0][-1]
            return rect, confidence #dlib.rectangle(rect[0], rect[1], rect[2], rect[3]), confidence
        return [], 0

            
