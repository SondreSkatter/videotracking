import cv2
import dlib

class FaceDetFrontal():
    predictor_model = 'models/weights/face_id/shape_predictor_68_face_landmarks.dat'
    face_rec_model_path = 'models/weights/face_id/dlib_face_recognition_resnet_model_v1.dat'

    def __init__(self):
        self.face_detector = dlib.get_frontal_face_detector()
        self.face_pose_predictor = dlib.shape_predictor(self.__class__.predictor_model)
        self.facerec = dlib.face_recognition_model_v1(self.__class__. face_rec_model_path)

    def execute(self, img):
        upSampleFactor = 2
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        detected_face,scores,idx = self.face_detector.run(gray, upSampleFactor, 0.0)
        if len(detected_face) > 0: 
            return img, detected_face[0], scores[0]
        return [], [], 0
