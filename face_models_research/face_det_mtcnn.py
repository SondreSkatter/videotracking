import mtcnn
import cv2

#https://pypi.org/project/mtcnn/
class FaceDetMTCNN:
    annotations_directory = 'face_det_results_mtcnn/'
    def __init__(self):
        self.detector = mtcnn.MTCNN()
    
    def execute(self, img):
        detections = self.detector.detect_faces(img)
        if detections:
            confidence = detections[0]['confidence']
            x1, y1, w, h = detections[0]['box']
            x2, y2 = x1 + w, y1 + h
            #print(confidence)

            #annotate image
            # img_annotation  = img.copy()
            # text = "{:.2f}%".format(confidence * 100)
            # y_label = y1 - 10 if y1 - 10 > 10 else y1 + 10
            # cv2.rectangle(img_annotation, (x1, y1), (x2, y2), (0, 0, 255), 1)
            # cv2.putText(img_annotation, text, (x1, y_label), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 1)
            # cv2.imwrite(self.__class__.annotations_directory + filename, img_annotation)

            #crop image
            img = img[y1:y2, x1:x2]
            return img
        #print('no face found')
        return []


            


