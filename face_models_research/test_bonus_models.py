import os
import glob
import cv2
import numpy as np 
import time
import argparse
import csv
import dlib

dataset = {
    'office': {
        'path': 'data/office/',
        'names': 'data/office_names.csv',
        'anchor_images': 
        {
            'sondre': '51-80-3b-13.png',
            'ryan' : '28-30-4a-6.png',
            'viet' : '45-65-0d-7.png' ,
            'lawrence' : '98-220-2a-28.png' ,
        },
    },
    'celebrity':
    {
        'path': 'data/celebrity/',
        'names': 'data/celebrity_names.csv',
        'anchor_images': 
        {
            'jennifer': 'jen1.jpeg',
            'nicholas' : 'nic1.jpeg',
            'halle' : 'Halle_Berry_0006.jpg',
            'clint' : 'Clint_Eastwood_0005.jpg',
        }
    }
}

def l2_normalize(x):
    return x / np.sqrt(np.sum(np.multiply(x, x)))

def get_distance(anchor_face, face_vector):
    return np.sqrt(np.sum(np.square(np.subtract(np.asarray(anchor_face), np.asarray(face_vector)))))
    #return np.linalg.norm(np.asarray(anchor_face) - np.asarray(face_vector))

#the mtcnn, cnn and dnn models don't return a perfect square
def calculate_square_rectangle(rect):
    height = rect.bottom() - rect.top()
    width = rect.right() - rect.left()
    sq = max(height, width)
    cx = rect.left() + width // 2
    cy = rect.top() + height // 2
    return dlib.rectangle(cx - sq // 2, cy - sq // 2, cx + sq // 2, cy + sq // 2)

if __name__ == "__main__":
    from models.detection.mtcnntf import FaceDetMTCNNTF
    from models.recognition.facenettf import FacenetTF

    detection = FaceDetMTCNNTF()
    recognition = FacenetTF()
    dataset_name = 'office'
    data =  dataset[dataset_name]
    
    path = os.path.join(os.getcwd(),data['path'])

    names_dict = dict()
    reader = csv.reader(open(os.path.join(os.getcwd(),data['names']), 'r'))
    for row in reader:
        k, v = row
        names_dict[k] = v

    anchor_dict = data['anchor_images']
    anchor_images = dict()
    for name, file_name in anchor_dict.items():
        input_img = cv2.imread(path + file_name)
        rect, confidence = detection.execute(input_img)
        if len(rect):
            result = recognition.execute(input_img, rect)
            anchor_images[name] = result

    num_of_detections = 0
    recognition_results = list()
    for file_name in os.listdir(path): 
        print(file_name)
        name = names_dict[file_name]
        input_img = cv2.imread(path + file_name)
        rect, confidence = detection.execute(input_img)
        if len(rect): #isinstance(rect, dlib.rectangle):
            num_of_detections+=1
            result = recognition.execute(input_img, rect)
            # img_annotation = input_img.copy()
            # cv2.rectangle(img_annotation, (rect.left(), rect.top()), (rect.right(), rect.bottom()), (0, 0, 255), 1)
            # cv2.imwrite('data/cropped/mtcnntf/' +  dataset_name + '/' + file_name, img_annotation)
            for anchor_name, anchor_image in anchor_images.items():
                dist = get_distance(anchor_image, result)
                print(dist)
                recognition_results.append((dist, 1 if name == anchor_name else 0))
    with open('results/%s_%s_%s_%s.csv' % ('mtcnntf', 'facenettf', 'office', str(num_of_detections)), 'w+') as out:
        csv_writer = csv.writer(out)
        csv_writer.writerow(['distance','identification'])
        for row in recognition_results:
            csv_writer.writerow(row)