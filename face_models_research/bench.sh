#!/bin/bash
DETECTION_MODELS=${DETECTION_MODELS:-mtcnn cnn dnn frontal}
RECOGNITION_MODELS=${RECOGNITION_MODELS:-openface facenet resnet}
DATASETS=${DATASETS:-office}

for det_model in $DETECTION_MODELS; do
    for rec_model in $RECOGNITION_MODELS; do
        for dataset in $DATASETS; do
            python3.7 recognition_results.py "$det_model" "$rec_model" "$dataset"
        done
    done
done